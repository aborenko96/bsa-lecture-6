<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['status', 'original_photo', 'photo_100_100', 'photo_150_150', 'photo_250_250'];

    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
