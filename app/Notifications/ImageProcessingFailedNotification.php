<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;

class ImageProcessingFailedNotification extends Notification
{
    use Queueable;

    private $user;
    public $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = $photo->user;
    }

    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage([
            'status' => 'fail'
        ]);
    }

    public function broadcastOn()
    {
        return new PrivateChannel("App.User." . $this->user->id);
    }
}
