<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Storage;
use Illuminate\Notifications\Messages\MailMessage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    private $user;
    public $photo;

    public function __construct(Photo $photo)
    {
        $this->user = $photo->user;
        $this->photo = $photo;
    }

    public function broadcastOn()
    {
        return new PrivateChannel("App.User." . $this->user->id);
    }

    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line("Dear {$this->user->name},")
            ->line("Photos have been successfully uploaded and processed.")
            ->line("Here are links to the images:")
            ->line(url(Storage::url($this->photo->photo_100_100)))
            ->line(url(Storage::url($this->photo->photo_150_150)))
            ->line(url(Storage::url($this->photo->photo_250_250)))
            ->line("Thanks!");
    }

    public function toBroadcast()
    {
        return new BroadcastMessage([
            'status' => 'success',
            'photo_100_100' => Storage::url($this->photo->photo_100_100),
            'photo_150_150' => Storage::url($this->photo->photo_150_150),
            'photo_250_250' => Storage::url($this->photo->photo_250_250)
        ]);
    }
}
