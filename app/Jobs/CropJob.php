<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Events\ImageProcessed;
use App\Events\ImageProcessedEvent;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Storage;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $user;
    public $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
        $this->user = $photo->user;
    }


    public function handle()
    {
        try {
            $service = app()->make(\App\Services\Contracts\PhotoService::class);
            $photo = $this->photo;

            $photo->status = "PROCESSING";
            $photo->save();

            $original = $photo->original_photo;

            $filename = pathinfo($original, PATHINFO_FILENAME);
            $extension = pathinfo($original, PATHINFO_EXTENSION);

            $user = $this->user;

            $savePath = "images/{$user->id}";

            $sizes = [
                [
                    'width' => 100,
                    'height' => 100
                ],
                [
                    'width' => 150,
                    'height' => 150
                ],
                [
                    'width' => 250,
                    'height' => 250
                ]
            ];

            foreach ($sizes as $size) {
                if (Storage::exists("$savePath/{$filename}{$size['width']}x{$size['height']}.$extension")) {
                    Storage::delete("$savePath/{$filename}{$size['width']}x{$size['height']}.$extension");
                }

                $path = "$savePath/{$filename}{$size['width']}x{$size['height']}.$extension";

                Storage::move($service->crop($original, $size['width'], $size['height']), $path);

                $prop = "photo_{$size['width']}_{$size['height']}";

                $photo->$prop = $path;
            }

            $photo->status = "SUCCESS";

            $photo->save();

            $user->notify(new ImageProcessedNotification($photo));
        } catch (\Throwable $e) {
            $this->failed();
        }
    }

    public function failed()
    {
        $this->photo->status = "FAIL";
        $this->photo->save();

        $this->user->notify(new ImageProcessingFailedNotification($this->photo));
    }
}
