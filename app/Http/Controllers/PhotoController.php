<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use App\Jobs\CropJob;;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PhotoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        $file = $request->file('photo');
        $pathName = $file->getPathname();
        $originalName = $file->getClientOriginalName();
        $user = auth()->user();

        $savePath = "images/{$user->id}";

        $path = Storage::putFileAs($savePath, new File($pathName), $originalName);

        $photo = new Photo([
            'status' => 'UPLOADED',
            'original_photo' => $path,
            'photo_100_100' => '',
            'photo_150_150' => '',
            'photo_250_250' => ''
        ]);

        $photo->user()->associate($user);

        $photo->save();

        CropJob::dispatch($photo);

    }
}
